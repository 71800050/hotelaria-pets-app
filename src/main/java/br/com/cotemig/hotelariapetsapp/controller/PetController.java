package br.com.cotemig.hotelariapetsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.hotelariapetsapp.model.Pet;
import br.com.cotemig.hotelariapetsapp.repository.PetRepository;

@Controller
public class PetController {
	
	@Autowired
	private PetRepository petrepository;
	
	@RequestMapping(value = "/cadastrarPet", method = RequestMethod.GET)
	public String form() {
		return "pets/formPet";
	}
	
	@RequestMapping(value = "/cadastrarPet", method = RequestMethod.POST)
	public String form(Pet pet) {
		petrepository.save(pet);
		return "redirect:/cadastrarPet";
	}
	
	@RequestMapping("/pets")
	public ModelAndView listaPets() {
		ModelAndView mv = new ModelAndView("index");
		Iterable<Pet> pets = petrepository.findAll();
		mv.addObject("pets", pets);
		return mv;
	}
}
