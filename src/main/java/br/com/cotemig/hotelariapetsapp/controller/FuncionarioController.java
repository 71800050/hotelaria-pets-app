package br.com.cotemig.hotelariapetsapp.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.hotelariapetsapp.model.Funcionario;
import br.com.cotemig.hotelariapetsapp.repository.FuncionarioRepository;

/**
 * @author José Eustaquio Muniz
 *
 */
@Controller
public class FuncionarioController {

	@Autowired
	private FuncionarioRepository funcionarioRepository;

	@RequestMapping(value = "/cadastrarFuncionario", method = RequestMethod.GET)
	public String form() {
		return "funcionarios/formFuncionario";
	}

	@RequestMapping(value = "/cadastrarFuncionario", method = RequestMethod.POST)
	public String form(Funcionario funcionario) {
		funcionarioRepository.save(funcionario);

		return "redirect:/cadastrarFuncionario";
	}
	
	@RequestMapping(value = "/atualizarFuncionario", method = RequestMethod.GET)
	public String formUpd() {
		return "funcionarios/updFuncionario";
	}
	
	@RequestMapping("/funcionarios")
	public ModelAndView listaFuncionarios() {
		ModelAndView mv = new ModelAndView("index");
		Iterable<Funcionario> funcionarios = funcionarioRepository.findAll();
		mv.addObject("funcionarios", funcionarios);

		return mv;
	}
	
	@PutMapping("/funcionarios/{id}")
	public ResponseEntity<Funcionario> atualizar(@PathVariable Long id,
			@RequestBody Funcionario funcionario) {
		Funcionario existente = funcionarioRepository.findById(id).get();

		if (existente == null) {
			return ResponseEntity.notFound().build();
		}

		BeanUtils.copyProperties(funcionario, existente, "id");

		existente = funcionarioRepository.save(existente);

		return ResponseEntity.ok(existente);
	}

	@DeleteMapping("/funcionarios/{id}")
	public ResponseEntity<Void> remover(@PathVariable Long id) {
		Funcionario existente = funcionarioRepository.findById(id).get();

		if (existente == null) {
			return ResponseEntity.notFound().build();
		}
		
		funcionarioRepository.delete(existente);
		
		return ResponseEntity.noContent().build();
	}

}
