package br.com.cotemig.hotelariapetsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.hotelariapetsapp.model.Pessoa;
import br.com.cotemig.hotelariapetsapp.repository.PessoaRepository;

@Controller
public class PessoaController {
	
	@Autowired
	private PessoaRepository pessoarepository;
	
	@RequestMapping(value = "/cadastrarPessoa", method = RequestMethod.GET)
	public String form() {
		return "pessoas/formPessoa";
	}
	
	@RequestMapping(value = "/cadastrarPessoa", method = RequestMethod.POST)
	public String form(Pessoa pessoa) {
		pessoarepository.save(pessoa);
		return "redirect:/cadastrarPessoa";
	}
	
	@RequestMapping("/pessoas")
	public ModelAndView listaPessoas() {
		ModelAndView mv = new ModelAndView("index");
		Iterable<Pessoa> pessoas = pessoarepository.findAll();
		mv.addObject("pessoas", pessoas);
		return mv;
	}
}
