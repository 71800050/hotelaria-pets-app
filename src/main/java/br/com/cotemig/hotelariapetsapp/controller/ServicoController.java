package br.com.cotemig.hotelariapetsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.hotelariapetsapp.model.Servico;
import br.com.cotemig.hotelariapetsapp.repository.ServicoRepository;

@Controller
public class ServicoController {

	@Autowired
	private ServicoRepository servicorepository;
	
	@RequestMapping(value = "/cadastrarServico", method = RequestMethod.GET)
	public String form() {
		return "servicos/formServico";
	}
	
	@RequestMapping(value = "/cadastrarServico", method = RequestMethod.POST)
	public String form(Servico servico) {
		servicorepository.save(servico);
		return "redirect:/cadastrarServico";
	}
	
	@RequestMapping("/servicos")
	public ModelAndView listaPets() {
		ModelAndView mv = new ModelAndView("index");
		Iterable<Servico> servicos = servicorepository.findAll();
		mv.addObject("servicos", servicos);
		return mv;
	}
}
