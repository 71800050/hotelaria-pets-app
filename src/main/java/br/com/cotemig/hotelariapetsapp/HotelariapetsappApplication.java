package br.com.cotemig.hotelariapetsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelariapetsappApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelariapetsappApplication.class, args);
	}

}
