/**
 * 
 */
package br.com.cotemig.hotelariapetsapp.service;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import br.com.cotemig.hotelariapetsapp.model.Funcionario;
import br.com.cotemig.hotelariapetsapp.repository.FuncionarioRepository;

/**
 * @author Jose Eustaquio Muniz
 *
 */
@Service
public class FuncionarioService implements ApplicationRunner {

	private FuncionarioRepository funcionarioRepository;

	public FuncionarioService(FuncionarioRepository funcionarioRepository) {
        this.funcionarioRepository = funcionarioRepository;
    }

	@Override
	public void run(ApplicationArguments args) throws Exception {

		var func = new Funcionario();
		func.setMatricula("39121524");
		func.setNome("João Pé de Feijão");
		func.setEmail("teste@gmail.com");
		func.setEndereco("Rua Teste, 378, Lourdes");
		func.setGrupo("Admin");
		func.setAtivo(true);

		funcionarioRepository.save(func);
	}

}
