package br.com.cotemig.hotelariapetsapp.service;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import br.com.cotemig.hotelariapetsapp.model.Pessoa;
import br.com.cotemig.hotelariapetsapp.repository.PessoaRepository;

@Service
public class PessoaService implements ApplicationRunner{
	
	private PessoaRepository pessoarepository;
	
    public PessoaService(PessoaRepository pessoarepository) {
        this.pessoarepository = pessoarepository;
    }

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		var pessoa = new Pessoa();
		pessoa.setNome("João Pé de Feijão");
		pessoa.setEmail("teste@gmail.com");
		pessoa.setEndereco("Rua Teste, 378, Lourdes");
		
		pessoarepository.save(pessoa);
	}
}
