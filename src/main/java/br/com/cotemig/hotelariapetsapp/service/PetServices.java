package br.com.cotemig.hotelariapetsapp.service;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import br.com.cotemig.hotelariapetsapp.model.Pet;
import br.com.cotemig.hotelariapetsapp.repository.PetRepository;


@Service
public class PetServices implements ApplicationRunner{
	
	private PetRepository petrepository;
	
    public PetServices(PetRepository petrepository) {
        this.petrepository = petrepository;
    }

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		var pet = new Pet();
		pet.setNomePet("testinho cabuloso");
		pet.setRaca("testinho");
		pet.setCor("Preta");
		pet.setDonoPet("Teste");
		
		petrepository.save(pet);		
	}
}
