package br.com.cotemig.hotelariapetsapp.service;

import java.util.Locale;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import br.com.cotemig.hotelariapetsapp.model.Servico;
import br.com.cotemig.hotelariapetsapp.repository.ServicoRepository;

@Service
public class ServicoService implements ApplicationRunner{

	private ServicoRepository servicorepository;
	
    public ServicoService(ServicoRepository servicorepository) {
        this.servicorepository = servicorepository;
    }

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		Locale.setDefault(Locale.US);
		
		var servico = new Servico();
		servico.setCodigo("0000001");
		servico.setDescricao("teste serviço");
		servico.setPreco(200.00);
		
		servicorepository.save(servico);
	}
}
