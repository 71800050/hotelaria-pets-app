package br.com.cotemig.hotelariapetsapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_pet")
public class Pet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PET", nullable = false)
	private Long id;
	private String nomePet;
	private String raca;
	private String cor;
	private String donoPet;
	
	public Pet(){
		
	}
	
	public Pet(Long id, String nomePet, String raca, String cor, String donoPet) {
		super();
		this.id = id;
		this.nomePet = nomePet;
		this.raca = raca;
		this.cor = cor;
		this.donoPet = donoPet;
	}

	public String getNomePet() {
		return nomePet;
	}

	public void setNomePet(String nomePet) {
		this.nomePet = nomePet;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDonoPet() {
		return donoPet;
	}

	public void setDonoPet(String donoPet) {
		this.donoPet = donoPet;
	}
}
