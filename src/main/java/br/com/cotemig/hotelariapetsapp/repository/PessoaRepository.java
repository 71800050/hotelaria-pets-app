package br.com.cotemig.hotelariapetsapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.hotelariapetsapp.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

}
