package br.com.cotemig.hotelariapetsapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.hotelariapetsapp.model.Pet;

public interface PetRepository extends JpaRepository<Pet, Long>{

}
