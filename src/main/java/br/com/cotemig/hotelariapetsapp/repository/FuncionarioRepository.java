/**
 * 
 */
package br.com.cotemig.hotelariapetsapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.hotelariapetsapp.model.Funcionario;

/**
 * @author José Eustaquio Muniz
 *
 */
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
	//Funcionario findByEmail(String email);
	
	//Funcionario findByMatricula(String matricula);
}
