package br.com.cotemig.hotelariapetsapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.hotelariapetsapp.model.Servico;

public interface ServicoRepository extends JpaRepository<Servico, Long>{

}
